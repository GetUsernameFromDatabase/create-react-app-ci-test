import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          I'm here <code>src/App.jsx</code>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>
          That was pretty <span style={{ color: "red" }}>fast</span>, tbh <br />
          Some might even say - <span style={{ color: "blue" }}>React</span>ive
        </p>
      </header>
    </div>
  );
}

export default App;
